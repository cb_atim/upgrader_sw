#include "iostream"

#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <termios.h>

#include <time.h>
#include <sys/wait.h>

#if defined(BOARD_RPI)
#include "rpi.h"
#elif defined(BOARD_RPI2)
#include "rpi2.h"
#endif

#if defined GPIO_WIRINGPI_CONTROLLED
#include <wiringPi.h>
#endif

/* default GPIO <-> upgrade board connections */
#define DEFAULT_NANO_nMCLR				18	// default gpio for MCLR line
#define DEFAULT_NANO_GND				4	// default gpio GND line
#define DEFAULT_NANO_TX					15	// default gpio TX nano line
#define DEFAULT_NANO_RX					14	// default gpio TX nano line

#define DEFAULT_PIC18_nMCLR				18	// default gpio for MCLR line
#define DEFAULT_PIC18_GND				15	// default gpio GND line
// #define DEFAULT_PIC18_PGC				4	// default gpio for PGC line
// #define DEFAULT_PIC18_PGD				14	// default gpio for PGD line

#define DEFAULT_LDO_ENABLE				7

#define DEFAULT_SELECT_NANO				1
#define DEFAULT_LED_GREEN_NANO			22
#define DEFAULT_LED_YELLOW_NANO			17
#define DEFAULT_LED_RED_NANO			27

#define DEFAULT_SELECT_PIC18			0
#define DEFAULT_LED_GREEN_PIC18			10
#define DEFAULT_LED_YELLOW_PIC18		9
#define DEFAULT_LED_RED_PIC18			11


int nano_gnd = DEFAULT_NANO_GND;
// int nano_nreset = DEFAULT_NANO_nMCLR;
int nano_tx = DEFAULT_NANO_TX;
int nano_rx = DEFAULT_NANO_RX;

int pic18_gnd = DEFAULT_PIC18_GND;
int pic18_nreset = DEFAULT_PIC18_nMCLR;
// int pic18_PGC = DEFAULT_PIC18_PGC;
// int pic18_PGD = DEFAULT_PIC18_PGD;

int ldo_enable = DEFAULT_LDO_ENABLE;

int select_nano = DEFAULT_SELECT_NANO;
int nano_led_green = DEFAULT_LED_GREEN_NANO;
int nano_led_yellow = DEFAULT_LED_YELLOW_NANO;
int nano_led_red = DEFAULT_LED_RED_NANO;

int select_pic18 = DEFAULT_SELECT_PIC18;
int pic18_led_green = DEFAULT_LED_GREEN_PIC18;
int pic18_led_yellow = DEFAULT_LED_YELLOW_PIC18;
int pic18_led_red = DEFAULT_LED_RED_PIC18;

int                 mem_fd;
void                *gpio_map;
volatile uint32_t   *gpio;

/*
 * delay:
 *	Wait for some number of milliseconds
 *********************************************************************************
 */

void delay( unsigned int howLong )
{
  struct timespec sleeper, dummy;

  sleeper.tv_sec  = (time_t)( howLong / 1000 ) ;
  sleeper.tv_nsec = (long)( howLong % 1000 ) * 1000000 ;

  nanosleep( &sleeper, &dummy );
}

// /* Hardware delay function by Gordon's Projects - WiringPi */
// void delay_us( unsigned int howLong )
// {
// 	struct timeval tNow, tLong, tEnd;

// 	gettimeofday( &tNow, 0 );
// 	tLong.tv_sec  = howLong / 1000000;
// 	tLong.tv_usec = howLong % 1000000;
// 	timeradd( &tNow, &tLong, &tEnd );

// 	while( timercmp( &tNow, &tEnd, < ) )
// 	{
// 		gettimeofday( &tNow, 0 );
// 	}
// }

void delay_ms( unsigned int howLong )
{
	// unsigned int i;
	// for( i = 0; i < howLong; i++ )
	// {
	// 	delay_us( 1000 );
	// }

	delay( howLong );
}

/* Set up a memory regions to access GPIO */
void setup_io( void )
{
	#if !defined GPIO_WIRINGPI_CONTROLLED

		/* open /dev/mem */
		mem_fd = open( "/dev/gpiomem", O_RDWR|O_SYNC );
		if( mem_fd == -1 )
		{
			perror( "Cannot open /dev/gpiomem" );
			exit( 1 );
		}

		/* mmap GPIO */
		gpio_map = mmap( 0, BLOCK_SIZE, PROT_READ|PROT_WRITE,
						MAP_SHARED, mem_fd, GPIO_BASE );
		if( gpio_map == MAP_FAILED )
		{
			perror( "mmap() failed" );
			exit( 1 );
		}

		/* Always use volatile pointer! */
		gpio = (volatile uint32_t *) gpio_map;
	#else
		wiringPiSetupGpio( );
	#endif

	delay_ms(100);        // sleep for 1us after GPIO configuration
}

/* Release GPIO memory region */
void close_io( void )
{
	int ret;

	#if !defined GPIO_WIRINGPI_CONTROLLED
		/* munmap GPIO */
		ret = munmap( gpio_map, BLOCK_SIZE );
		if( ret == -1 )
		{
			perror( "munmap() failed" );
			exit( 1 );
		}

		/* close /dev/mem */
		ret = close( mem_fd );
		if( ret == -1 )
		{
			perror( "Cannot close /dev/mem" );
			exit( 1 );
		}

		delay_ms(100);		// CBT
	#else
	#endif
}

// int reset_port( void )
// {
// 	int fd; /* File descriptor for the port */

// 	fd = open( "/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY );

// 	if( fd == -1 )
// 	{
// 		// Could not open the port.          
// 		perror( "open_port: Unable to open /dev/serial0 - " );
// 	}
// 	else
// 	{
// 		fcntl( fd, F_SETFL, 0 );
// 	}

// 	sleep( 10 );	//required to make flush work, for some reason
// 	tcflush( fd, TCIOFLUSH );
// 	sleep( 10 );
// 	close( fd );

// 	return ( fd );
// }

void set_blocking( int fd, int should_block )
{
		struct termios tty;
		memset( &tty, 0, sizeof tty );
		if( tcgetattr( fd, &tty ) != 0 )
		{
			//error ("error %d getting term settings set_blocking", errno);
			perror( "error getting term settings set_blocking" );    
			return;
		}

		tty.c_cc[VMIN]  = should_block ? 1 : 0;
		tty.c_cc[VTIME] = should_block ? 5 : 0; // 0.5 seconds read timeout

		if( tcsetattr( fd, TCSANOW, &tty ) != 0 )
		{
			perror( "error setting term blocking" );
		}

		//error ("error setting term %sblocking", should_block ? "" : "no");
}

int reset_port_block( void )
{
	int fd = open( "/dev/ttyS0", O_RDWR | O_NOCTTY | O_SYNC );

	if( fd < 0 )
	{
		perror( "open_port: Unable to open /dev/ttyS0 - " );
		exit( 1 );
	}
	set_blocking( fd, 0 );   // disable reads blocked when no input ready

	char buf [1024];
	int n;

	do{
		n = read( fd, buf, sizeof buf );
	} while( n > 0 );

	set_blocking( fd, 1 );  // enable read blocking (if desired)

	close( fd );

	return( fd );
}


// demo.C - Sample C++ prgoram 
int main( void )
{
	int i;

	struct{
	   int select_nano_level = 0;
	   int select_pic18_level  = 0;
	   int ds30_nano_process_child_created = 0;
	   int pb_retry = 0;
	   int notify = 0;
	}flags;

	enum{
		dsSTOP,
		dsRUN,
		dsRUN_FAILED,
		dsINIT,
		dsINIT_FAILED,
		dsPARSE,
		dsPARSE_FAILED,
		dsOPEN,
		dsOPEN_FAILED,
		dsSEARCH,
		dsSEARCH_FAILED,
		dsFOUND,
		dsFOUND_FAILED,
		dsWRITE,
		dsWRITE_FAILED,
		dsGLOBAL_FAILURE,
		dsEND,
	}ds30process=dsSTOP;

	enum{
		pbSTOP,
		pbRUN,
		pbINIT,
		pbSEARCH,
		pbSEARCH_FAILED,
		pbWRITE,
		pbWRITE_FAILED,
		pbGLOBAL_FAILURE,
		pbEND,
	}picberryprocess=pbSTOP;

	char buffer[1024];
	std::string line;
	std::ifstream input;

	int pipefd[2];
	int status;

	pid_t pid;

	char *const ds30args[] = {
		(char *)"/home/pi/projects/ds30loader/ds30SecureLoaderConsole.exe",
		(char *)"-f=/home/pi/projects/ds30loader/ARM-N8-LP-V5836_encrypted.hex",
		(char *)"-d=PIC24F32KA304",
		(char *)"-k=/dev/ttyS0",
		(char *)"-r=57600",
		// (char *)"-a=330",
		(char *)"-a=500",
		// (char *)"--ht=60000",
		(char *)"--ht=10000",
		// (char *)"--timeout=60000",
		(char *)"--timeout=10000",
		(char *)"--writef",
		(char *)"-o",
		(char *)"--no-goto-app",
		(char *)"--no-goto-bl",
		NULL };

	char *const picberryargs[] = {
		(char *)"/home/pi/projects/picberry/picberry",
		(char *)"-w",
		(char *)"/home/pi/projects/picberry/firmwares/ACW_TH_V4.06.hex",
		(char *)"-g",
		(char *)"4,14,18",
		(char *)"-f",
		(char *)"pic18fj",
		// (char *)"--noverif",
		NULL };

	std::cout << "\nUPGRADER build 08062017, 17h07\n";

	while(1)
	{
		setup_io( );

		#if defined GPIO_WIRINGPI_CONTROLLED
			pinMode( nano_gnd, INPUT );

			pinMode( ldo_enable, OUTPUT );
			digitalWrite( ldo_enable, LOW );

			pinMode( pic18_nreset, OUTPUT );
			digitalWrite( pic18_nreset, LOW );

			pinMode( nano_led_green, OUTPUT );
			digitalWrite( nano_led_green, LOW );
			pinMode( nano_led_yellow, OUTPUT );
			digitalWrite( nano_led_yellow, LOW );
			pinMode( nano_led_red, OUTPUT );
			digitalWrite( nano_led_red, LOW );

			pinMode( pic18_led_green, OUTPUT );
			digitalWrite( pic18_led_green, LOW );
			pinMode( pic18_led_yellow, OUTPUT );
			digitalWrite( pic18_led_yellow, LOW );
			pinMode( pic18_led_red, OUTPUT );
			digitalWrite( pic18_led_red, LOW );

			pinMode( select_nano, INPUT );
			pinMode( select_pic18, INPUT );

			flags.select_nano_level = ( digitalRead( select_nano ) == 0 );
			flags.select_pic18_level = ( digitalRead( select_pic18 ) == 0 );
			// std::cerr << "\nWiringPi lib => select_nano :" << digitalRead(select_nano) <<" select_pic18 :" << digitalRead(select_pic18) <<"\n";
		#else
			GPIO_IN(nano_gnd );

			GPIO_IN(ldo_enable );
			GPIO_OUT(ldo_enable );
			GPIO_CLR(ldo_enable );

			GPIO_IN(pic18_nreset );
			GPIO_OUT(pic18_nreset );
			GPIO_CLR(pic18_nreset );

			// Leds
			GPIO_IN( nano_led_green );
			GPIO_IN( nano_led_yellow );
			GPIO_IN( nano_led_red );
			GPIO_IN( pic18_led_green );
			GPIO_IN( pic18_led_yellow );
			GPIO_IN( pic18_led_red );

			GPIO_OUT( nano_led_green );
			GPIO_OUT( nano_led_yellow );
			GPIO_OUT( nano_led_red );
			GPIO_OUT( pic18_led_green );
			GPIO_OUT( pic18_led_yellow );
			GPIO_OUT( pic18_led_red );

			GPIO_CLR( nano_led_green );
			GPIO_CLR( nano_led_yellow );
			GPIO_CLR( nano_led_red );
			GPIO_CLR( pic18_led_green );
			GPIO_CLR( pic18_led_yellow );
			GPIO_CLR( pic18_led_red );

			GPIO_IN( select_nano );
			GPIO_IN( select_pic18 );

			flags.select_nano_level = ( GPIO_LEV( select_nano ) == 0 );
			flags.select_pic18_level = ( GPIO_LEV( select_pic18 ) == 0 );
			// std::cerr << "\nGPIO mmap handling => select_nano :" << GPIO_LEV(select_nano) <<" select_pic18 :" << GPIO_LEV(select_pic18) <<"\n";
		#endif

		flags.notify = 1;

		if( flags.select_nano_level == 1 )
		{
			std::cout << "\nNANO update selected\n";
		}
		else if( flags.select_pic18_level == 1 )
		{
			std::cout << "\nHOST update selected\n";
		}
		else
		{
			std::cout << "\nNONE update selected\n";
		}

		delay_ms( 2000 );        // sleep for 1us after GPIO configuration

		if( flags.select_nano_level == 1 )
		{
			//reset_port( );
			//reset_port_block( );

			//close_io( );
			//return 0;

			pipe( pipefd );

			pid = fork( ); /* Create a child process */

			if( pid == 0 )
			{
				close( pipefd[0] );    // close reading end in the child

				dup2( pipefd[1], 1 );  // send stdout to the pipe
				dup2( pipefd[1], 2 );  // send stderr to the pipe

				close( pipefd[1] );    // this descriptor is no longer needed

				printf( "This is the child process. My pid is %d and my parent's id is %d.\n", getpid( ), getppid( ) );

				/* Lancement de la commande */
				int a = execv( "/home/pi/projects/ds30loader/ds30SecureLoaderConsole.exe", ds30args ); 

				while( 1 );

				std::cerr << "Uh-Oh! execv() failed, code :" << a << "\n"; /* execl doesn't return unless there's an error */
				exit( 1 );
			}
			else if( pid < 0 )            // failed to fork
			{
				std::cout << "Failed to fork\n";
				exit( 1 );
			}
			else
			{
				// parent
				printf( "This is the parent process. My pid is %d and my child's id is %d.\n", getpid( ), pid );

				close( pipefd[1] );  // close the write end of the pipe in the parent

				std::cout << "executes process parent\n";

				std::cout << "\tgoto dsRUN\n";
				ds30process = dsRUN;

				while( 1 )
				{

					#if defined GPIO_WIRINGPI_CONTROLLED
					flags.select_nano_level = ( digitalRead( select_nano ) == 0 );
					flags.select_pic18_level = ( digitalRead( select_pic18 ) == 0 );
					// std::cerr << "\nWiringPi lib => select_nano :" << digitalRead(select_nano) <<" select_pic18 :" << digitalRead(select_pic18) <<"\n";
					#else
					flags.select_nano_level = ( GPIO_LEV( select_nano ) == 0 );
					flags.select_pic18_level = ( GPIO_LEV( select_pic18 ) == 0 );
					// std::cerr << "\nGPIO mmap handling => select_nano :" << GPIO_LEV(select_nano) <<" select_pic18 :" << GPIO_LEV(select_pic18) <<"\n";
					#endif

					if( flags.select_nano_level != 1 )
					{
						std::cout << "\tgoto dsSTOP\n";
						ds30process = dsSTOP;
					}

					switch( ds30process )
					{
						case dsSTOP:


						case dsRUN:
									// std::cout << "dsRUN\n";
									reset_port_block( );

									#if defined GPIO_WIRINGPI_CONTROLLED
									// std::cout << "GPIO_WIRINGPI_CONTROLLED\n";
									pinMode( nano_gnd, INPUT) ;
									digitalWrite( nano_gnd, LOW );

									pinMode( ldo_enable, OUTPUT );
									digitalWrite( ldo_enable, LOW );

									pinMode( pic18_nreset, OUTPUT );
									digitalWrite( pic18_nreset, LOW );

									digitalWrite( nano_led_green, LOW );
									digitalWrite( nano_led_yellow, HIGH );
									digitalWrite( nano_led_red, LOW );

									#else

									// std::cout << "!!! GPIO_WIRINGPI_CONTROLLED none !!!\n";
									GPIO_IN( nano_gnd );
									GPIO_OUT( nano_gnd );
									GPIO_CLR( nano_gnd );

									GPIO_IN( pic18_nreset );
									GPIO_OUT( pic18_nreset );
									GPIO_CLR( pic18_nreset );

									GPIO_IN( ldo_enable );
									GPIO_OUT( ldo_enable );
									GPIO_CLR( ldo_enable );

									GPIO_CLR( nano_led_green );
									GPIO_SET( nano_led_yellow  );
									GPIO_CLR( nano_led_red );

									#endif

									memset( buffer, '\0', sizeof(buffer) );

									if( ds30process != dsSTOP )
									{
										std::cout << "\tgoto dsINIT\n";
										ds30process = dsINIT;	
									}
									break;

						case dsRUN_FAILED:
										// std::cout << "dsRUN_FAILED\n";
										break;

						case dsINIT:
										// std::cout << "dsINIT\n";
										/* Read the output a line at a time - output it. */
										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )//
										{
											if( strstr( buffer, (const char *)"ds30 Loader device database is initialized") != NULL )
											{
												memset( buffer, '\0', sizeof(buffer) );
												std::cout << "\tgoto dsPARSE\n";
												ds30process = dsPARSE;
											}
										}
										break;

						case dsINIT_FAILED:
										// std::cout << "dsINIT_FAILED\n";
										std::cout << "\tgoto dsGLOBAL_FAILURE\n";
										ds30process = dsGLOBAL_FAILURE;
										break;

						case dsPARSE:
										// std::cout << "dsPARSE\n";
										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if( strstr( buffer, (const char *)"Hex file successfully parsed") != NULL )
											{
												#if defined GPIO_WIRINGPI_CONTROLLED
												digitalWrite( ldo_enable, HIGH );
												#else
												GPIO_SET( ldo_enable );
												#endif
												memset( buffer, '\0', sizeof(buffer) );
												std::cout << "\tgoto dsOPEN\n";
												ds30process = dsOPEN;
											}
										}
										break;

						case dsPARSE_FAILED:
										// std::cout << "dsPARSE_FAILED\n";
										ds30process = dsGLOBAL_FAILURE;
										break;

						case dsOPEN:
										// std::cout << "dsOPEN\n";
										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if( strstr( buffer, (const char *)"Failed to open port") != NULL )
											{
												std::cout << "Failed to open com port\n";
												std::cout << "\tgoto dsOPEN_FAILED\n";
												ds30process = dsOPEN_FAILED;
											}
											if( strstr( buffer, (const char *)"Searching for bl ") != NULL )
											{
												std::cout << "Port COM open, searching...\n";
												// #if defined GPIO_WIRINGPI_CONTROLLED
												// digitalWrite( ldo_enable, HIGH );
												// #else
												// GPIO_SET(ldo_enable);
												// #endif
												memset( buffer, '\0', sizeof(buffer) );
												std::cout << "\tgoto dsSEARCH\n";
												ds30process = dsSEARCH;
											}
											printf( "%s\n", buffer );
										}
										break;

						case dsOPEN_FAILED:
										// std::cout << "dsOPEN_FAILED\n";
										std::cout << "\tgoto dsGLOBAL_FAILURE\n";
										ds30process = dsGLOBAL_FAILURE;
										break;

						case dsSEARCH:
										// std::cout << "dsSEARCH\n";

										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( nano_led_yellow, HIGH );
										delay_ms( 50 );
										digitalWrite( nano_led_yellow, LOW );
										delay_ms( 200 );
										#else
										GPIO_SET( nano_led_yellow );
										delay_ms( 50 );
										GPIO_CLR( nano_led_yellow );
										delay_ms( 200 );
										#endif

										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if( strstr( buffer, (const char *)"Waiting for the boot loader to be ready...ok") != NULL )
											{
												memset( buffer, '\0', sizeof(buffer) );
												std::cout << "\tgoto dsFOUND\n";
												ds30process = dsFOUND;
												break;
											}
											if(strstr (buffer, (const char *)"unknown device") != NULL
												|| strstr( buffer, (const char *)"timed out") != NULL
												|| strstr( buffer, (const char *)"fail") != NULL )
											{
												std::cout << "\tgoto dsSEARCH_FAILED\n";
												ds30process = dsSEARCH_FAILED;
											}
										}
										break;

						case dsFOUND_FAILED:
										// std::cout << "dsFOUND_FAILED\n";
						case dsSEARCH_FAILED:
										// std::cout << "dsSEARCH_FAILED\n";
										std::cout << "\tgoto dsGLOBAL_FAILURE\n";
										ds30process = dsGLOBAL_FAILURE;
										break;

						case dsFOUND:
										// std::cout << "dsFOUND\n";
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( nano_led_yellow, HIGH );
										digitalWrite( nano_led_green, HIGH );
										delay_ms( 50 );
										digitalWrite( nano_led_yellow, LOW );
										digitalWrite( nano_led_green, LOW );
										delay_ms( 200 );
										#else
										GPIO_SET( nano_led_yellow );
										GPIO_SET( nano_led_green );
										delay_ms( 50 );
										GPIO_CLR( nano_led_yellow );
										GPIO_CLR( nano_led_green );
										delay_ms( 200 );
										#endif

										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if(strstr( buffer, (const char *)"Writing flash." ) != NULL)
											{
												memset( buffer, '\0', sizeof(buffer) );
												std::cout << "\tgoto dsWRITE\n";
												ds30process = dsWRITE;
											}
											if(strstr( buffer, (const char *)"Write failed" ) != NULL 
												|| strstr( buffer, (const char *)"timed out" ) != NULL
												|| strstr( buffer, (const char *)"fail" ) != NULL )
											{
												std::cout << "Found failed\n";
												std::cout << "\tgoto dsFOUND_FAILED\n";
												ds30process = dsFOUND_FAILED;
											}
										}
										break;

						case dsWRITE:
										// std::cout << "dsWRITE\n";
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( nano_led_green, HIGH );
										delay_ms( 50 );
										digitalWrite( nano_led_green, LOW );
										delay_ms( 50 );
										#else
										GPIO_SET( nano_led_green );
										delay_ms( 50 );
										GPIO_CLR( nano_led_green );
										delay_ms( 50 );
										#endif

										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if( strstr( buffer, (const char *)"Write successfully completed" ) )
											{
												std::cout << "Write successfully completed!!\n";

												#if defined GPIO_WIRINGPI_CONTROLLED
												digitalWrite( nano_led_green, HIGH );
												#else
												GPIO_SET(nano_led_green);
												#endif
												delay_ms( 3000 );

												std::cout << "\tgoto dsEND\n";
												ds30process = dsEND;
											}

											if( strstr( buffer, (const char *)"Write failed" ) != NULL
												|| strstr( buffer, (const char *)"timed out" ) != NULL )
											{
												std::cout << "Write failed\n";

												std::cout << "\tgoto dsWRITE_FAILED\n";
												ds30process = dsWRITE_FAILED;
											}
										}
										break;

						case dsWRITE_FAILED:
										// std::cout << "dsWRITE_FAILED\n";
										std::cout << "\tgoto dsGLOBAL_FAILURE\n";
										ds30process = dsGLOBAL_FAILURE;
										break;

						case dsGLOBAL_FAILURE:
										// std::cout << "dsGLOBAL_FAILURE\n";
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( ldo_enable, LOW );

										digitalWrite( nano_led_red, LOW );
										digitalWrite( nano_led_yellow, LOW );
										digitalWrite( nano_led_green, LOW );
										#else
										GPIO_CLR( ldo_enable );

										GPIO_CLR( nano_led_red );
										GPIO_CLR( nano_led_yellow );
										GPIO_CLR( nano_led_green );
										#endif

										/* close */
										std::cout << "Global failure \n";
										printf( "%s\n", buffer );

										for( i=0; i<8; i++ )
										{
											#if defined GPIO_WIRINGPI_CONTROLLED
											digitalWrite( nano_led_red, HIGH );
											delay_ms( 200 );
											digitalWrite( nano_led_red, LOW );
											delay_ms( 200 );
											#else
											GPIO_SET( nano_led_red );
											delay_ms( 200 );
											GPIO_CLR( nano_led_red );
											delay_ms( 200 );
											#endif
										}
						case dsEND:
										// std::cout << "dsEND\n";
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( ldo_enable, LOW );
										#else
										GPIO_CLR( ldo_enable );
										#endif
										printf( "kill pid %d\n", pid );
										kill(pid, SIGTERM);
										std::cout << "wait end of child process \n";
										delay_ms( 500 );

										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( nano_led_red, LOW );
										digitalWrite( nano_led_yellow, LOW );
										digitalWrite( nano_led_green, LOW );
										#else
										GPIO_CLR( nano_led_red );
										GPIO_CLR( nano_led_yellow  );
										GPIO_CLR( nano_led_green );
										#endif

										while( wait( &status ) != pid );

										std::cout << "\tgoto dsSTOP\n";
										ds30process = dsSTOP;
										break;
					}

					if( ds30process == dsSTOP )
					{
						std::cout << "dsSTOP\n";
						std::cout << "Upgrade process for NANO exited with " << WIFEXITED(status) << "\n";//WEXITSTATUS
                        delay_ms( 3000 );
						break;	//stop searching through pipe
					}
				}
			}
		}
		else if( flags.select_pic18_level == 1 )
		{
			pipe( pipefd );

			pid = fork( ); /* Create a child process */

			if( pid == 0 )
			{
				close( pipefd[0] );    // close reading end in the child

				dup2( pipefd[1], 1 );  // send stdout to the pipe
				dup2( pipefd[1], 2 );  // send stderr to the pipe

				close( pipefd[1] );    // this descriptor is no longer needed

				printf( "This is the child process. My pid is %d and my parent's id is %d.\n", getpid( ), getppid( ) );

				/* Lancement de la commande */
				int a = execv( "/home/pi/projects/picberry/picberry", picberryargs ); 

				std::cerr << "Uh-Oh! execv() failed, code :" << a << "\n"; /* execl doesn't return unless there's an error */
				exit( 1 );
			}
			else if( pid < 0 )            // failed to fork
			{
				std::cout << "Failed to fork\n";
				exit( 1 );
			}
			else
			{
				// parent
				printf( "This is the parent process. My pid is %d and my parent's id is %d.\n", getpid( ), pid );

				close( pipefd[1] );  // close the write end of the pipe in the parent

				std::cout << "executes process parent\n";

				memset( buffer, '\0', sizeof(buffer) );

				picberryprocess = pbRUN;

				while( 1 )
				{
					#if defined GPIO_WIRINGPI_CONTROLLED
						flags.select_nano_level = ( digitalRead( select_nano ) == 0 );
						flags.select_pic18_level = ( digitalRead( select_pic18 ) == 0 );
					// std::cerr << "\nWiringPi lib => select_nano :" << digitalRead(select_nano) <<" select_pic18 :" << digitalRead(select_pic18) <<"\n";
					#else
						flags.select_nano_level = ( GPIO_LEV( select_nano ) == 0 );
						flags.select_pic18_level = ( GPIO_LEV( select_pic18 ) == 0 );
					// std::cerr << "\nGPIO mmap handling => select_nano :" << GPIO_LEV(select_nano) <<" select_pic18 :" << GPIO_LEV(select_pic18) <<"\n";
					#endif

					if( flags.select_pic18_level != 1 )
					{
						std::cout << "\tgoto pbSTOP\n";
						picberryprocess = pbSTOP;
					}

					switch( picberryprocess )
					{
						case pbSTOP:
						case pbRUN:
									#if defined GPIO_WIRINGPI_CONTROLLED
									pinMode( ldo_enable, OUTPUT );
									digitalWrite( ldo_enable, HIGH );

									// pinMode( pic18_gnd, OUTPUT );
									// digitalWrite( pic18_gnd, LOW );

									// pinMode( pic18_nreset, OUTPUT );
									// digitalWrite( pic18_nreset, HIGH );

									digitalWrite( pic18_led_green, LOW );
									digitalWrite( pic18_led_yellow, HIGH );
									digitalWrite( pic18_led_red, LOW );
									#else
									GPIO_IN( ldo_enable );
									GPIO_OUT( ldo_enable );
									GPIO_SET( ldo_enable );

									// GPIO_IN( pic18_gnd );
									// GPIO_OUT( pic18_gnd );
									// GPIO_CLR( pic18_gnd );

									// GPIO_IN( pic18_nreset );
									// GPIO_OUT( pic18_nreset );
									// GPIO_SET( pic18_nreset );

									GPIO_CLR( pic18_led_green );
									GPIO_SET( pic18_led_yellow  );
									GPIO_CLR( pic18_led_red );
									#endif

									flags.pb_retry = 0;
									memset( buffer, '\0', sizeof(buffer) );

									if( picberryprocess != pbSTOP )
									{
										std::cout << "\tgoto pbINIT\n";
										picberryprocess = pbINIT;	
									}
									break;

						case pbINIT:
									//picberry PIC Programmer
									if( read( pipefd[0], buffer, sizeof(buffer) ) != 0)
									{
										printf( "%s\n", buffer );

										if( strstr( buffer, (const char *)"Device Name: PIC18F26J53" ) != NULL )
										{
											memset( buffer, '\0', sizeof(buffer) );
											picberryprocess = pbSEARCH;
										}
										if( strstr( buffer, (const char *)"programmer not connected")  != NULL )
										{
											//std::cout << "Global failure \n";
											//printf( "%s\n", buffer );

											#if defined GPIO_WIRINGPI_CONTROLLED
											digitalWrite( pic18_led_yellow, LOW );
											#else
											GPIO_CLR( pic18_led_yellow );
											#endif

											flags.pb_retry = 1;
											picberryprocess = pbGLOBAL_FAILURE;	//retry
										}
									}
									break;

						case pbSEARCH:
									#if defined GPIO_WIRINGPI_CONTROLLED
									digitalWrite( pic18_led_yellow, HIGH );
									delay_ms( 50 );
									digitalWrite( pic18_led_yellow, LOW );
									delay_ms( 200 );
									#else
									GPIO_SET(pic18_led_yellow );
									delay_ms( 50 );
									GPIO_CLR(pic18_led_yellow );
									delay_ms( 200 );
									#endif

									if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
									{
										printf( "%s\n", buffer );

										if( strstr( buffer, (const char *)"fail" ) != NULL 
											|| strstr( buffer, (const char *)"timed out ") != NULL
											|| strstr( buffer, (const char *)"error" ) != NULL
											|| strstr( buffer, (const char *)"exit" ) != NULL )
										{
											picberryprocess = pbGLOBAL_FAILURE;
											break;
										}

										if( strstr( buffer, (const char *)"Writing chip") != NULL )
										{
											memset( buffer, '\0', sizeof(buffer) );
											picberryprocess = pbWRITE;
											break;
										}


									}
									break;

						case pbWRITE:
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( pic18_led_green, HIGH );
										delay_ms( 50 );
										digitalWrite( pic18_led_green, LOW );
										delay_ms( 50 );
										#else
										GPIO_SET( pic18_led_green );
										delay_ms( 50 );
										GPIO_CLR( pic18_led_green );
										delay_ms( 50 );
										#endif

										if( read( pipefd[0], buffer, sizeof(buffer) ) != 0 )
										{
											if( strstr( buffer, (const char *)"fail" ) != NULL 
												|| strstr( buffer, (const char *)"timed out" ) != NULL
												|| strstr( buffer, (const char *)"error" ) != NULL
												|| strstr( buffer, (const char *)"Error" ) != NULL
												|| strstr( buffer, (const char *)"Exit" ) != NULL )
											{
												picberryprocess = pbWRITE_FAILED;
												break;
											}
											else if( strstr( buffer, (const char *)"DONE!" ) )
											{
												std::cout << "Write successfully completed!!\n";

												#if defined GPIO_WIRINGPI_CONTROLLED
												digitalWrite( pic18_led_green, HIGH );
												#else
												GPIO_SET( pic18_led_green );
												#endif
												delay_ms( 3000 );

												picberryprocess = pbEND;
												break;
											}
										}

										break;

						case pbSEARCH_FAILED:
						case pbWRITE_FAILED:
										picberryprocess = pbGLOBAL_FAILURE;
										break;

						case pbGLOBAL_FAILURE:
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( ldo_enable, LOW );

										digitalWrite( pic18_led_red, LOW );
										digitalWrite( pic18_led_yellow, LOW );
										digitalWrite( pic18_led_green, LOW );
										#else
										GPIO_CLR( ldo_enable );

										GPIO_CLR( pic18_led_red );
										GPIO_CLR( pic18_led_yellow );
										GPIO_CLR( pic18_led_green );
										#endif

										if( flags.pb_retry == 0 )
										{
											/* close */
											std::cout << "Global failure \n";
											printf( "%s\n", buffer );

											for( i=0; i<8; i++ )
											{
												#if defined GPIO_WIRINGPI_CONTROLLED
												digitalWrite( pic18_led_red, HIGH );
												delay_ms( 200 );
												digitalWrite( pic18_led_red, LOW );
												delay_ms( 200 );
												#else
												GPIO_SET( pic18_led_red );
												delay_ms( 200 );
												GPIO_CLR( pic18_led_red );
												delay_ms( 200 );
												#endif
											}
										}

						case pbEND:
										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( ldo_enable, LOW );
										#else
										GPIO_CLR( ldo_enable );
										#endif
										printf( "kill pid %d\n", pid );
										kill(pid, SIGTERM);
										std::cout << "wait end of child process \n";

										delay_ms( 500 );

										#if defined GPIO_WIRINGPI_CONTROLLED
										digitalWrite( pic18_led_red, LOW );
										digitalWrite( pic18_led_yellow, LOW );
										digitalWrite( pic18_led_green, LOW );
										#else
										GPIO_CLR( pic18_led_red );
										GPIO_CLR( pic18_led_yellow  );
										GPIO_CLR( pic18_led_green );
										#endif

										while( wait( &status ) != pid );

										picberryprocess = pbSTOP;

										break;
					}

					if( picberryprocess == pbSTOP )
					{
						std::cout << "Upgrade Process for Host exited with " << WIFEXITED(status) << "\n";//WEXITSTATUS
                        delay_ms( 3000 );
						break;
					}
				}
			}
		}
		else
		{
			delay_ms( 10000 );
		}

		close_io( );
	}

	return 0;
}
